###*
 * read it later
 * chrome extension
 * @author  Michal Haták <me@twista.cz>
###

###*
 * collection of items
 * @type {Object}
###
data = {}

###*
 * index reference
 * @type {int}
###
lastIndex = 0

###*
 * load data from chrome synchronized storage and proccess
###
load = ()->
    chrome.storage.sync.get 'items', loadFromStorage

###*
 * process loaded data
 * @param  {Object} items
###
loadFromStorage = (items)->
    if items['items']
        counter = 0
        for key, value of items['items']
            console.log key
            console.log value
            data[key] = value
            counter++
        lastIndex = counter

    if counter == 0
        data = {}
        lastIndex = 0
    return

###*
 * save data into synchronized storage
 * @param  {Function} cb callback
###
saveToStorage = (cb) ->
        wut = {items: data}
        chrome.storage.sync.set wut, () ->
            if (cb && typeof(cb) == "function")
                cb()

###*
 * add item to collection
 * @param {Tab}   tab
###
add = (tab, cb) ->
    item = {url: tab.url, name: tab.title}
    lastIndex++
    data[lastIndex] = item
    saveToStorage(cb)
    load()
    return

###*
 * remove item from collection
 * @param  {int} id
###
remove = (id)->
    delete data[id]
    normalize()
    saveToStorage()

###*
 * normalize data
###
normalize = ()->
    counter = 0
    temp = {}
    for key,item of data
        temp[counter] =
            url: item.url
            name: item.name
        counter++
    data = temp
    return



###*
 * context menu callback
 * @param  {Object} info
 * @param  {Object} tab
 * @return {bool}
###
contextMenuCallback = (info, tab) ->
    item = {url: info.pageUrl, title: tab.title}
    add item, notifiyBadge
    return

###*
 * show notificaton
###
notifiyBadge = () ->
    chrome.browserAction.setBadgeBackgroundColor { color: [0, 255, 0, 255] }
    chrome.browserAction.setBadgeText { text: "OK!" }
    setTimeout (->
        chrome.browserAction.setBadgeText {text: ""}),
    2000



###*
 * register context menu callback
###
chrome.contextMenus.create ({"title": chrome.i18n.getMessage( "context_title"), "onclick": contextMenuCallback})


# init ril - read data from storage
load()

###*
 * register runtime message listener
 * @param  {Object} request      message
 * @param  {MessageSender} sender
 * @param  {function} sendResponse message callback
###
chrome.runtime.onMessage.addListener (request,sender,sendResponse) ->

    switch request.message
        when "getItems"
            sendResponse {items : data}
            break

        when "addItem"
            chrome.tabs.query { active: true, currentWindow: true }, (tab) ->
                tab = tab[0]
                add tab, notifiyBadge
                return
            sendResponse {msg: 'ok'}
            break

        when "removeItem"
            remove request.itemkey
            sendResponse {msg: 'ok'}
            break

        else
            sendResponse({})
