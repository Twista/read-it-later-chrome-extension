#Read it later! - chrome extension

read it later is chrome extension, that allows you save favorites websites (sites you wanna read later)

**extension aviable at chrome [store](https://chrome.google.com/webstore/detail/read-it-later/aaocbkeamabaniccpnbapflopmcnpjbg)**

read it later doesn't show ads, doesn't need any login or whatever. just install and profit

read it later is completly free

## Feature list

* key shortcut
* graphic improvements
* user-settings
* shortcuts via omnibox

## Changelog

* 0.9.0
    + completly rewritten into CoffeeScript
    + become's open-source
* 0.8.3
    + some fixes in PL translation
* 0.8.2
    + Polish translation (thanks to Andrzej Bartosik)
* 0.8.1
    + Slovenian translantion (thanks to Domen Skamlič)
* 0.8.0
    + english translation fix ("delete all")
    + minority improvements
* 0.7.1
    + minority changes (notification via Badge + typo)
* 0.7.0
    + remove add link notification (alert)
    + styles
* 0.5.0
    + display favicon
    + display website name (you have to re-save old records)
    + link form website name
    + german translation (thanks to [Matti Maier](http://mattimaier.de))
* 0.3.0
    + first version (PoC)

## Bugs

if you find any kind of bug or you miss some feature, feel free to mail me :)